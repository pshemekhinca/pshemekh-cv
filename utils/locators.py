class Navbar:
    """ Navigation Bar web elements """
    about_button_xpath = '//*[@id="about-btn"]'
    skills_button_xpath = '//*[@id="skills-btn"]'
    projects_button_xpath = '//*[@id="projects-btn"]'
    work_button_xpath = '//*[@id="work-btn"]'
    education_button_xpath = '//*[@id="edu-btn"]'
    contact_button_xpath = '//*[@id="contact-btn"]'


class Sections:
    """ SPA sections elements """
    # SECTIONS HEADERS
    name_head_xpath = '//*[@id="name"]'
    about_head_xpath = '//*[@id="about"]//h1'
    skills_head_xpath = '//*[@id="skills"]//h1'
    projects_head_xpath = '//*[@id="projects"]//h1'
    work_head_xpath = '//*[@id="work"]//h1'
    education_head_xpath = '//*[@id="education"]//h1'
    contact_head_xpath = '//*[@id="contact"]//h1'

