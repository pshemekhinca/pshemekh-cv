import os
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from utils.locators import Navbar, Sections
import time


class FunctionalTests(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        if os.name == 'nt':
            chromedriver_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), r"libs\chromedriver.exe")
            self.driver = webdriver.Chrome(executable_path=chromedriver_path)
            print('WARNING! Before tests run in terminal "cd cvportfolio" and then "python -m http.server"')
            base_url = 'http://localhost:8000/'
        else:
            chrome_options = webdriver.ChromeOptions()
            chrome_options.add_argument('--no-sandbox')
            chrome_options.add_argument('--headless')
            chrome_options.add_argument('--disable-gpu')
            self.driver = webdriver.Chrome(options=chrome_options)
            base_url = 'https://pshemekh-cv-staging.vercel.app'


        self.driver.get(base_url)
        self.go_to_element(self, '//*[@id="contact"]')

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    def test_correct_redirection_to_About_page_section(self):
        expected_header_text = "About me"
        self.click_button(Navbar.about_button_xpath)
        header = self.driver.find_element_by_xpath(Sections.about_head_xpath)
        self.assertEqual(expected_header_text, header.text,
                         f'Expected header text differ from the {self.driver.current_url} header')

    def test_correct_redirection_to_Skills_page_section(self):
        expected_header_text = "Technical skills"
        self.click_button(Navbar.skills_button_xpath)
        header = self.driver.find_element_by_xpath(Sections.skills_head_xpath)
        self.assertEqual(expected_header_text, header.text,
                         f'Expected header text differ from the {self.driver.current_url} header')

    def test_correct_redirection_to_Projects_page_section(self):
        expected_header_text = "My\nProjects"
        self.click_button(Navbar.projects_button_xpath)
        header = self.driver.find_element_by_xpath(Sections.projects_head_xpath)
        self.assertEqual(expected_header_text, header.text,
                         f'Expected header text differ from the {self.driver.current_url} header')

    def test_correct_redirection_to_Work_page_section(self):
        expected_header_text = "Selected\nwork experience"
        self.click_button(Navbar.work_button_xpath)
        header = self.driver.find_element_by_xpath(Sections.work_head_xpath)
        self.assertEqual(expected_header_text, header.text,
                         f'Expected header text differ from the {self.driver.current_url} header')

    def test_correct_redirection_to_Education_page_section(self):
        expected_header_text = "Education\nbackground"
        self.click_button(Navbar.education_button_xpath)
        header = self.driver.find_element_by_xpath(Sections.education_head_xpath)
        self.assertEqual(expected_header_text, header.text,
                         f'Expected header text differ from the {self.driver.current_url} header')

    def test_correct_redirection_to_Contact_page_section(self):
        expected_header_text = "Contact Me"
        self.click_button(Navbar.contact_button_xpath)
        header = self.driver.find_element_by_xpath(Sections.contact_head_xpath)
        self.assertEqual(expected_header_text, header.text,
                         f'Expected header text differ from the {self.driver.current_url} header')


    def get_element_text_value(self, element_locator):
        """Gets text value of web element
                :param element_locator: webpage element xpath
                :return: None
        """
        self.visibility_of_element_wait(self.driver, element_locator)
        return self.driver.find_element_by_xpath(element_locator).text

    def click_button(self, button_xpath):
        """Click action on given button xpath element
                :param button_xpath: given button xpath
        """
        self.visibility_of_element_wait(self.driver, button_xpath)
        self.driver.find_element_by_xpath(button_xpath).click()
        time.sleep(1)

    def visibility_of_element_wait(self, driver, xpath, timeout=10) -> WebElement:
        """Check if element specified by xpath is visible on page
                  :param driver: webdriver or event firing webdriver instance
                  :param xpath:  web element xpath
                  :param timeout: after timeout waiting will be stopped (default: 10)
                  :return found element
        """
        locator = (By.XPATH, xpath)
        element_located = EC.presence_of_element_located(locator)

        if hasattr(driver, 'wrapped_driver'):
            unwrapped_driver = driver.wrapped_driver
            wait = WebDriverWait(unwrapped_driver, timeout)
        else:
            wait = WebDriverWait(driver, timeout)
        return wait.until(element_located, f"Element for xpath: '{xpath}'and url: {driver.current_url} not found")

    def go_to_element(self, xpath):
        """Scrolls web page to searched element
                :param xpath: web element xpath (in assertion, element text value)
        """
        time.sleep(3)
        element = self.driver.find_element_by_xpath(xpath)
        actions = ActionChains(self.driver)
        actions.move_to_element(element).perform()
        time.sleep(2)
