import os
import unittest
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
import time


class SmokeTests(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        if os.name == 'nt':
            chromedriver_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), r"libs\chromedriver.exe")
            self.driver = webdriver.Chrome(executable_path=chromedriver_path)
            print('WARNING! Before tests run in terminal "cd cvportfolio" and then "python -m http.server"')
            base_url = 'http://localhost:8000/'
        else:
            chrome_options = webdriver.ChromeOptions()
            chrome_options.add_argument('--no-sandbox')
            chrome_options.add_argument('--headless')
            chrome_options.add_argument('--disable-gpu')
            self.driver = webdriver.Chrome(options=chrome_options)
            base_url = 'https://pshemekh-cv-staging.vercel.app'

        self.driver.get(base_url)

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    def test_correct_welcome_name(self):
        expected_name = 'Przemek\nHinca'
        xpath = '//*[@id="name"]'

        self.assert_element(expected_name, xpath)

    def test_my_photo_is_visible(self):
        photo_xpath = '//*[@id="my-photo"]'

        self.assert_if_element_is_visible(photo_xpath)

    def test_download_button_is_visible(self):
        button_xpath = '//*[@id="cv-download"]'

        self.assert_if_element_is_visible(button_xpath)

    def test_skill_cards_number(self):
        expected_number = '12'
        cards_xpath = '//*[@class="skill-card"]'
        page_space = '//*[@class="grid"]'

        self.assert_elements_number(expected_number, cards_xpath, page_space)

    def test_project_cards_number(self):
        expected_number = '7'
        projects_xpath = '//*[@class="portfolio_item"]'
        page_space = '//*[@class="main-container portfolio-inner clearfix"]'

        self.assert_elements_number(expected_number, projects_xpath, page_space)

    def test_selected_work_places_number(self):
        expected_number = '5'
        works_xpath = '//*[@id="work"]//*[@class="timeline-event-copy"]'
        page_space = '//*[@id="work"]'

        self.assert_elements_number(expected_number, works_xpath, page_space)

    def test_education_places_number(self):
        expected_number = '7'
        edu_xpath = '//*[@id="education"]//*[@class="timeline-event-copy"]'
        page_space = '//*[@id="education"]'

        self.assert_elements_number(expected_number, edu_xpath, page_space)

    def test_correct_email_address(self):
        expected_email = 'pshemek.hi@gmail.com'
        xpath = '//*[@id="my-email"]'
        self.go_to_element(xpath)

        self.assert_element(expected_email, xpath)


    def test_sections_headers_number(self):
        expected_number = '7'
        header_xpath = '//h1'
        page_space = '//*[@id="contact"]'

        self.assert_elements_number(expected_number, header_xpath, page_space)

    def assert_element(self, expected_value, xpath):
        """Checks presence of specific web element
                :param expected_value: value expected
                :param xpath: web element xpath (in assertion, element text value)
        """
        web_element = self.driver.find_element_by_xpath(xpath)
        self.assertEqual(expected_value, web_element.text)

    def assert_if_element_is_visible(self, element_xpath):
        """Checks if web element is displayed
                :param element_xpath: element xpath to look for
        """
        self.assertTrue(self.driver.find_element_by_xpath(element_xpath), f'Element not found')

    def go_to_element(self, xpath):
        """Scrolls web page to searched element
                :param xpath: web element xpath (in assertion, element text value)
        """
        time.sleep(3)
        element = self.driver.find_element_by_xpath(xpath)
        actions = ActionChains(self.driver)
        actions.move_to_element(element).perform()
        time.sleep(2)

    def assert_elements_number(self, expected_number, elements_xpath, area_xpath):
        """Checks presence of specific web element
                :param expected_number: value expected
                :param elements_xpath: web elements xpath
                :param area_xpath: web page area xpath, to scroll and load elements in selected page part
        """
        self.go_to_element(area_xpath)
        number = len(self.driver.find_elements_by_xpath(elements_xpath))

        self.assertEqual(expected_number, str(number))
